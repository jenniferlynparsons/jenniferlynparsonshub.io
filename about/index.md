---
layout: page
title: About Jennifer
tags: [about]
date: 2016-09-16
comments: false
---
    
<p class="center">coder. writer. comic geek. old crone. tea snob. vegetarian.</p>

<hr>

<div class="center">
    <a class="btn zoombtn" href="http://jenniferlynparsons.net/">Resume</a>
    <a class="btn zoombtn" href="http://pixelpaperyarn.rocks">Project Hub</a>
</div>

Jennifer Lyn Parsons is a senior web developer from the beautiful, beloved state of New Jersey. She currently works for [Lab Zero Innovations](https://labzero.com/), a digital consultancy based in San Francisco, working in various front end technologies and building tools for Fortune 10 clients.

With over 7 years of professional web development experience and an additional decade of graphic design work under her belt, Jennifer has expertise in a wide variety of obscure and often obtuse technologies. This often comes in handy when she's trying to learn *new* obscure and obtuse technologies.

Her goal as a developer is to build tools that are useful, engaging, elegant and leave the world a better place than she found it. Her personal projects such as [selfcare.tech](http://selfcare.tech) and her text-based game engine, [Plastic](https://github.com/jenniferlynparsons/plastic), are all rooted in this core mission.

When not coding things, she runs a small but vibrant publishing company, [Luna Station Press](http://www.lunastationpress.com/), writes essays on web development related topics and writes stories that have nothing to do with web development. 

Whatever downtime is left goes to video games, comics books, and making things out of wool and paper. 

Sometimes she sleeps.

<hr>

I do not claim mastery of my subject, but simply wish to share that which I have learned along the way, that it may proof useful to others is a side benefit. This quote by Montaigne sums up my feelings about this work's inspiration far better than I could:

"What I write here is not my teaching, but my study; it is not a lesson for others, but for me. And yet it should not be held against me if I publish what I write. What is useful to me may also by accident be useful to another. Moreover, I am not spoiling anything, I am only using what is mine. And if I play the fool, it is at my expense and without harm to anyone. For it is a folly that will die with me, and will have no consequences."