---
layout: post
title:  "Software as a Tiny House"
date:   2017-12-04
tag:
- general thoughts
comments: false
---
I love the idea of living in a tiny house, something portable and just big enough to fit the essentials. While I don’t know if I would be happy living in one myself, I do love learning about them. 

I was watching some video tours of tiny houses recently and some of the things the folks who lived in and built them had to say resonated with me. However, it didn’t have anything to do with my own living situation. Instead, I found quite a few insights that could be applied to software and website architecture and design. In fact, just the idea that we use those same two terms in both house construction and building software makes me sure the concepts are a natural fit.

While I’m still pondering these connections, here’s what I gleaned so far from watching those tours:

## What do the people living there need? 
*(don’t build useless spaces, repurpose spaces that are underused)*

There are essentials, things that every house and piece of software needs to have to make it useable in general. There are always exceptions, but a kitchen, bath, bed, and living space are all important in a house, even if they all exist in the same room. The same can be said for software. An interface, storage, and functions that process the interactions between the data and the user, are all pretty standard in working software. 

It’s good to ask if a new space or functionality is necessary, and seeking new ways to repurpose the same things in other areas is key to making efficient use of both space and code.

## What’s the most economical way to do it? 
*(in energy & resources, not just money)*

Tiny houses are often built from recycled materials when possible. It saves cost and is also lower impact on the planet. While any app’s power efficiency (unless you’re Amazon or something) is likely to be small already, looking for ways to make the code run more efficiently is a good practice all around. 

Additionally, when thinking in terms of resources, it’s good to recognize the cost in people hours as well. Solid planning and an understanding of the problem you’re trying to solve will save time and help stem the tide of burnout that’s far too rampant in our industry.

## What’s healthiest for the whole person? 
*(make it simple to move through, don’t make things hard to reach, does it breathe)*

A tiny house that makes you climb into a loft doesn’t work if you have certain health issues. Large windows might be less energy efficient than small ones, but the benefits gained from the sense of space makes the trade off worth making.

With software, a strong designer is essential here. If things are difficult for a user to accomplish or are unintuitive or cramped, their experience will be lesser. In some cases, this can mean the loss of return visitors and for some sites and software, those users are critical to success. If your software is meant to be a help to others, than making it difficult to use puts an extra toll on someone who is already compromised.

## Get to know your space and what you need it to be. 
*(requires self-exploration as well as trial and error, make it what it needs to be but no more)*

For a tiny house, knowing that there needs to be a priority on the kitchen because the owner is a cook is a vital piece of information. They may ask for a full four-burner stove. Perhaps instead they really only need a two-burner and a portable hot plate, leaving room for a larger sink that will make their living experience more pleasurable in general.

Knowing who you are building your software for and anticipating their needs is not easy. It takes work and thought and research, but the end result will be a smoother experience.

## Be willing to let the materials guide the building. 
*(recycled materials, clever use of non-traditional tools, etc. will change everything)*

Tiny houses have limits on them. Size restrictions based on building code or road regulations delineate what is possible to build. That’s where creativity comes into play, maximizing the space in unexpected and clever ways.

Here an understanding of the tools at your disposal as a developer comes in handy. Rarely does any developer get to choose the libraries or languages they’re working in. Frequently a developer is asked to build something that a language doesn’t necessarily do well. A deep understanding of tools, of architecture, of the language of choice is a great asset in these situations, allowing for a creative use of the resources at hand and providing a satisfying challenge.

## A small footprint and low power requirements keep it sustainable. 
*(will it still work off grid?)*

Many tiny houses can work perfectly well off the grid. No power or water hookups needed, they are designed to work well when those options are unavailable. 

Software gets bloated easily. When space is virtually free and libraries are limitless in availability, there’s very little incentive to put any constraints on how the code is actually written. However, with each new layer of complexity from another library and every megabyte of storage used to hold the software, maintenance becomes an issue. Cost, too, can become troublesome, either in the form of server space, energy costs, and environmental impact over time. 

It’s also useful to ask if your software is still useful to someone who has no or limited access to the internet for some reason. What can be done to keep things running at low (or no) latency?

## Multi use, purpose built functional things solve some problems more efficiently. 
*(customization can maximize space and ease of use)*

Most things in a tiny house have multiple uses. Very often a couch will also be a storage bench, a table is also a desk. This maximizes both space and utility.

It’s a useful exercise, at least, to consider the benefits of writing a series of functions to fill a need rather than installing an entire new library of functions that may or may not be used. Testing to make sure that every area of a program is running most efficiently will help reveal code that can be reused or refactored. What functions can be made universal? 

## Ease of use and at hand vs the thought process behind it.
*(is everything within reach and as intuitive as possible?)*

A tiny house works best when its owner can move effortlessly through the space and use it as best pleases them. For some, an origami folding cabinet that houses layers of functionality works best. For others, it’s more comfortable to make as few changes to their space as possible day to day.

Just because a piece of software can do five different things if you change various settings doesn’t mean it should. Users shouldn’t have to think to hard about how to navigate through the tool in front of them. If complexity is required, then ease of use becomes a higher priority. 

The same goes with building the software itself. If a function is difficult to understand and use because it does many things at once, it is not “at hand”. The balance of making something easy lies in the difficulty in creating that ease. More thought is required before code is written and the initial effort’s worth has to be weight against the end result’s usability. 

## Beauty and a sense of space even when there isn’t much actual space is vital to livability. 
*(the culmination of all the planning and building needs to be a livable space that’s comfortable)*

Windows are a massive factor in a tiny house. Those with few or very small ones tend to feel tighter, more cramped, less livable. Beauty is an even more important factor here than in a larger home, where the lack can be compensated for with furniture, decor, and other additions.

Software, too, needs to have a sense of space and beauty in the both the interface and in the code itself. Cramming too much functionality into an interface makes it feel less useable. Writing code that tries to do too much with each function becomes not only fragile, but also difficult to debug and maintain, never mind adding new features.

All of the work put into building a piece of software, regardless of the platform, needs to result in a code base that’s maintainable and flexible and a UI/UX that meets a user’s needs and guides them through the process of getting there without unnecessary work or distraction.

I hope this exploration gave you something to think about as you sit down to write that next line of code. I know it has me thinking differently as I approach my day to day work and also as I prepare to start any new projects in the future. Compact, concise, beautiful, usable, the tiny house philosophy turns out to be a great way to write software, too.