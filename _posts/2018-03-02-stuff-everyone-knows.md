---
layout: post
title:  "Talk Notes: Stuff Everyone Knows Except You"
date:   2018-03-02
excerpt: "Laurie Voss, COO of npm, came to Hack Reactor to give his much beloved talk on things software engineers are expected to know but are rarely told."
tag:
- talk notes
comments: false
---

## Talk
[**Stuff Everyone Knows Except You, pt. 1**](https://www.youtube.com/watch?v=JIJZnF_L5KI): Laurie Voss, Hack Reactor `1:02:15`

[**Stuff Everyone Knows Except You, pt. 2**](https://www.youtube.com/watch?v=4H8VTCSbYQg): Laurie Voss, Hack Reactor `1:38:16`

## Summary
Laurie Voss, COO of npm, came to Hack Reactor to give his much beloved talk on things software engineers are expected to know but are rarely told. He expounds on a wide range of topics from the skills you should focus on first to how to plot out your software career. 


## My Notes
This was far and away one of the most useful talks I've ever watched. Up there with Rich Hickey's "Simple Made Easy". I love it. It was long. It was funny. It was fucking packed with information. Some I knew, some I didn't, and some I only had a vague idea about but now understand a lot better. Highly recommended and highly rewatchable.