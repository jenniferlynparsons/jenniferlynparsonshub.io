---
layout: post
title:  "Talk Notes: Learn to Code: How React.js Works"
date:   2018-02-20
excerpt: "This talk explains various elements of how React.js works."
tag:
- talk notes
comments: false
---

## Talk
[**Learn to Code: How React.js Works**](https://www.youtube.com/watch?v=mLMfx8BEt8g) by Fullstack Academy `11:23`

## Summary
This talk explains various elements of how React.js works. It goes over the virtual DOM, the diffing algorithm, optimizations that the diffing algorithm uses to speed up the process. There is also a bit on some tools that you can use to optimize and speed up your React app.

## My Notes
This was a nice quick overview of some of the magic behind how React works. Definitely gave me a bit of that context I'm always looking for.