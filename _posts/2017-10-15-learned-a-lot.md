---
layout: post
title:  "I learned a lot, but how much?"
date:   2017-10-15
tag:
- general thoughts
comments: false
---
We’re often told in programming that personal projects are, in addition to being a great way to build a portfolio of work to show prospective employers, a method for learning new things. That’s totally true. Personal projects, especially if we’re working solo, can provide challenges that will help us learn combined with the freedom to pursue this work at our own pace and in whatever area we’re interested in exploring.

So, how do we tackle a personal project?  We sit down, plan it, code it, tweet about it and then after boatload of work and the excitement of launching, we’re done, right?  I often have had the sense that I learned “so much” by working on something.

Sure. But what exactly have we learned? Has this been useful to us beyond the project existing for ourselves and others to use? How do we gauge the value of this experience in terms of our education and growth?

Fo me, the answer lies in something many of us who work in Agile paradigms are already familiar with, but with a slightly different focus: The retrospective.

Fairly recently I’ve started doing a retrospective session to see what I got out of doing a particular personal project. If you wanted, you could even do a solo sprint and retrospective pattern of work as well. These retrospectives help me to gain insights into what worked, what didn’t, etc. Surprising things turn up as I write about the project I just completed.

When I’m done I have an immediate and thorough understanding of what I’ve just done and what I learned along the way. An added benefit is that as long as I keep doing this, I’ll have a record I can look back on of what I was thinking in the moment and what the most distinct aspects of the project were for me at the time.

I also take a little time to reflect on what I learned about myself as a person in doing the project. What kind of choices I made, how I reacted to the challenges and hurdles I encountered, and what parts were satisfying or interesting to me. This can help lead my choices for future projects as well as areas that might be interesting to pursue in my study and work choices.

It does take a bit of extra time and effort to do this, but not overly much and it’s been worth doing so far. If you’d like to try it out yourself,  here’s my template to use as a starting point for your own:

- Challenges going into the project:

- What resources did I have to mitigate these challenges? (People, books, sites, etc)

- How did I prepare for the project?

- Challenges during the process:

- Where did I get really stuck?

- How did I overcome those hurdles?

- Could I have done it better and what would that have taken?

- If this project exposed knowledge gaps, what do I need to learn?

- What did I learn from this project?

  - Technologically:

  - Personally:
