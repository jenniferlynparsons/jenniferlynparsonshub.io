---
layout: post
title:  "Talk Notes: What the heck is the event loop anyway?"
date:   2018-02-21
excerpt: "With some handy visualisations, and fun hacks, let’s get an intuitive understanding of what happens when JavaScript runs."
tag:
- talk notes
comments: false
---

## Talk
[**What the Heck Is the Event Loop Anyway?**](https://www.youtube.com/watch?v=8aGhZQkoFbQ) by Philip Roberts @ JSConf.EU `26:53`

## Resources 
[Transcript](https://2014.jsconf.eu/speakers/philip-roberts-what-the-heck-is-the-event-loop-anyway.html)

## Summary

JavaScript programmers like to use words like, “event-loop”, “non-blocking”, “callback”, “asynchronous”, “single-threaded” and “concurrency”. We say things like “don’t block the event loop”, “make sure your code runs at 60 frames-per-second”, “well of course, it won’t work, that function is an asynchronous callback!” If you’re anything like me, you nod and agree, as if it’s all obvious, even though you don’t actually know what the words mean; and yet, finding good explanations of how JavaScript actually works isn’t all that easy, so let’s learn! With some handy visualisations, and fun hacks, let’s get an intuitive understanding of what happens when JavaScript runs.

## My Notes

I actually have the pleasure of a passing acquaintence with Philip Roberts, though I didn't realize it was him until I saw his avatar on one of his slides. lol. This was an incredibly useful talk and one I would highly recommend to anyone looking to learn more about how JavaScript actually works in the browser (which probably should be anyone who writes JS if I'm honest). I'll be coming back to this one for a repeat watching.