---
layout: post
title:  "If Elle Woods wrote code"
date:   2016-04-01
tag:
- general thoughts
comments: false
---

This is not a deep, incisive essay. Or at least I wasn’t intending it to be. But a bit of depth crept in anyway.

Coders love their IDEs (the software we write our code in, basically glorified text editors). It’s a known fact that most of us are pretty opinionated on which one is the “best”, with what makes one or the other “best” being up for serious interpretation. Additionally, most of this software is highly customizable. Themes are available all over the place to truly make your working environment your own.

Most of these customizations lean toward the dark, uber geeky variety. Lots of grey and black and Matrix terminal green. Maybe some blue or brown for variety.

But earlier this week I stumbled across (in other words, Darby Kim Thomas posted) a screenshot of a theme that broke that all wide open.

<a href="https://twitter.com/ddddarby/status/715248352963612673"><img src="../assets/img/posts/tweet.png" /></a>

See that? It’s girly, isn’t it? I am not a girly person in the least and yet something about the Barbie doll Dream House look of this theme has me seriously contemplating switching from my beloved SublimeText to the Atom IDE, just so I can use this pink and purple fantasia.

After seeing that, I started poking around a little and found another theme, which can be used on Atom, Sublime, and even Slack.

<img src="../assets/img/posts/wild-cherry.png" />
Wild Cherry Theme by Omar Maashal - https://github.com/mashaal/wild-cherry

The reaction by the other ladies in tech that I hang around with was a pretty immediate “Yes, OMG, Where do I get this piece of magic?” It’s girly (in that oh so stereotypical way), it’s gaudy, and yet we all wanted it. Now.

It got me thinking about how we view our tech tools and the kind of boxes we put ourselves into. It also got me thinking about Elle Woods and how quietly subversive such a simple thing like wearing (or dressing your IDE in) pink really is.

Pink (and purple, and pastel colors) is unexpected in the world of tech. It can be used to break open some pretty solid doors and open up conversations about what it means to be a woman in tech.

On a related note, there was a divisive article that passed my gaze earlier this week as well that made a claim that being a lesbian in tech was easier than being a straight woman. The claim was based on the thought that lesbians would be more in tune with guy energy and more willing to play paintball than the straight girls.

But that article, and the pink themes I’ve shown you, both bring up the same question. Do women have to become ‘one of the guys’ in order to succeed in tech?

I like to believe they don’t. If they do, they shouldn’t.

By bringing pink in, by not just being one of the guys, women can provide a lens into the work we all do that will improve our industry as well as the products we put out into the world. Hearts and minds can be opened and new possibilities can be uncovered that would not exist if we weren’t on your teams.

My point was driven home even more so just a few hours ago when my social media feeds started filling up with Powerpuff versions of everyone I know. I put the link on my work Slack and wrangled the whole team (90% men) into Powerpuffing themselves. They had a blast. It was great fun, and they all voiced how much they love the Powerpuff Girls.

These tiny, super-powered ladies brought a bunch of men to a standstill and it was all in the name of pure joy.

Interestingly enough, it was my Women in Tech Slack friends that were the ones that started picking at the Powerpuff tool, speculating on what was going on under the hood.

Which brings me around to the song I’ll keep singing until I don’t need to anymore: Diversity makes tech better. All the colors of the rainbow make tech better.

And one day you just might find yourself loving pink without irony.

<img src="../assets/img/posts/powerpuffme.jpg">
