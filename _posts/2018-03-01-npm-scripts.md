---
layout: post
title:  "Talk Notes: Advanced front-end automation with npm scripts"
date:   2018-03-01
excerpt: "Kate Hudson talks about simplifying front-end automation using tools we already have at hand."
tag:
- talk notes
comments: false
---

## Talk
[**Advanced front-end automation with npm scripts**](https://www.youtube.com/watch?v=0RYETb9YVrk): Kate Hudson, Nordic.js 2015 `17:49`

## Summary
Kate Hudson talks about simplifying front-end automation using tools we already have at hand.


## My Notes

This was interesting as it shifted the curriculum I wrote for myself a little bit. I had intended on learning Gulp, Grunt, etc. and while I'm sure I'll run into those tools somewhere again, for my personal projects I'm now more likely to use npm scripts. They're straightforward and build on knowledge I already have.