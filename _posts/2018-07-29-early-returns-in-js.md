---
layout: post
title:  "Early Returns in JavaScript"
date:   2018-07-29
excerpt: "A couple of articles related to how early returns work crossed my path recently, enough to pique my curiosity. Learning about early returns in JS has been not only a challenging technical exploration, but also pretty fun. In researching the concept, I remembered how much I enjoy learning something for it's own sake, not just as a means to an end."
tag:
- javascript
comments: false
---

A couple of articles related to how early returns work crossed my path recently, enough to pique my curiosity. Learning about early returns in JS has been not only a challenging technical exploration, but also pretty fun. In researching the concept, I remembered how much I enjoy learning something for it's own sake, not just as a means to an end. Sure, the information I gathered is useful and the technique is something I'll likely employ in the long run, but it was just as valuable enjoying following the trail of breadcrumbs that led to my final conclusion.

## What is an early return?

In short, an early return provides functionality so the result of a conditional statement can be returned as soon as a result is available, rather than wait until the rest of the function is run.

Surprisingly, I hadn't seen this kind of pattern in JS before, though I'm used to seeing it in Ruby, where it's quite common. I was curious as to why that was. A bit of research showed me that returning only after the rest of the code had run, whether it was needed or not, was a holdover from older imperative programming languages where it was required to return at the end of a function to ensure the code ran in the correct order.

The only answer I could really find to why I hadn't run into this pattern before is that even now this is not taught nor talked about enough for folks to start using it regularly, dropping the imperative style. A few newer devs have started the conversation and argue quite strongly that it's clearer to read.

## I still had questions

I was unsure about the readability of this style. I'm so used to reading if/else statements with a variable set at the top of the function that gets returned at the end. Having multiple returns or even multiple if statements (rather than if/else) felt like it would be more challenging to understand at a glance.

More important than readability, I wondered how performant it was versus the single point/end of function returns. My gut told me it was probably faster because, in theory, less code was being run. In researching this, I found a pre-2012 article hinting it might actually be less performant but didn't find any information after that supporting one style or the other.

It would be interesting to discover that a single return was faster and if it was, why that was so. I decided to run some benchmarks to see what our winner was.

## Benchmarking early returns

Here are my benchmark tests:

-   [JSBEN.CH Benchmarking for JavaScript](http://jsben.ch/5JJE5)
-   [JavaScript benchmark playground](http://jsbench.github.io/#1fd9788db451f01d4b84e12bba8d706f)

As you can see when you run the tests, there are only 10s of milliseconds difference in speed between the various options, fairly negligible. Additionally I get different results each time I run the benchmarks, with the old imperitive style even winning occasionally. Generally, it looks like "early return within an if/else" comes in a close second to "early returns with single statements" and both of those win over the old "single point of return" imperative method.

## Conclusion

In comparing these examples, I find that despite the multiple statements method winning, I prefer to use a single if/else for readability. It feels less cumbersome than setting up a variable just to have a return value as in single return. At the same time, having a series of statements inside a function as in the "early returns with single statements" version abstracts the fact that they're consequentially connected to each other and to me, that's less readable.

All in all I feel like I've gained a deeper grasp on some of JavaScript's inner workings, with the bonus of trying out some benchmarking and learning a bit of programming history as well.

## Resources

-   [Return early - Wilson Page](http://wilsonpage.co.uk/return-early/)

-   [Probably Wrong - Avoid Else, Return Early](http://blog.timoxley.com/post/47041269194/avoid-else-return-early)

-   [Should I return from a function early or use an if statement? - Software Engineering Stack Exchange](https://softwareengineering.stackexchange.com/questions/18454/should-i-return-from-a-function-early-or-use-an-if-statement)

-   [Code formatting and readability - lecterror](http://lecterror.com/articles/view/code-formatting-and-readability)

-   [Where did the notion of "one return only" come from? - Software Engineering Stack Exchange](https://softwareengineering.stackexchange.com/questions/118703/where-did-the-notion-of-one-return-only-come-from)

-   [The Return Early Pattern - Guide - The freeCodeCamp Forum](https://forum.freecodecamp.org/t/the-return-early-pattern/19364)

-   [design - Is returning early from a function more elegant than an if statement? - Stack Overflow](https://stackoverflow.com/questions/355670/is-returning-early-from-a-function-more-elegant-than-an-if-statement)

-   [Stylish Saturday: Early Returns](https://dev.to/howtocodejs/stylish-saturday-early-returns-722)
