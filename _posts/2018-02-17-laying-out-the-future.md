---
layout: post
title:  "Talk Notes: Laying out the Future with Grid and Flexbox"
date:   2018-02-17
excerpt: "CSS is now 20 years old, and it has taken this long for us to get a true layout system designed for web pages and applications."
tag:
- talk notes
comments: false
---

## Talk
[**Laying out the Future with Grid and Flexbox**](https://www.youtube.com/watch?v=ibeF6rbzD70) by Rachel Andrew @ View Source Conference (2016) `25:45`

## Resources 
[Rachel's Notes and links](https://rachelandrew.co.uk/speaking/event/view-source-conference-berlin)

## Summary

It is 2016. CSS is now 20 years old, and it has taken this long for us to get a true layout system designed for web pages and applications. A layout system, not based on hacking properties designed for something else entirely, but one that considers the reality of design for the web today.

## My Notes

This was an overview of the Grid, Flexbox and Alignment features that, in 2016 when this talk was presented, were still much more nascent than they are today. 

The talk was very in depth and I was hoping to get an overview of the features, but felt like a lot of it went by too quickly for me to grasp much, even as I tried to take notes. Fortunately, Rachel is really great with how she organizes her notes and resources on her talks on her website.

Because of how old this talk is (wtf that less than 2 years ago is outdated. lol) it feels much more like a sales pitch for the new modules rather than what I was looking to learn.