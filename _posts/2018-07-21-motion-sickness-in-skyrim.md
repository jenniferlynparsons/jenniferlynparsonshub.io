---
layout: post
title:  "Relieving motion sickness in Skyrim SE"
date:   2018-07-21
excerpt: "Skyrim is an old game, but I hadn't really played it. I had it for the Xbox 360, but all I had done was run around collecting cabbages. I never made it passed Whiterun. It wasn't that I was bored, particularly, but other games held my attention a bit better."
tag:
- video games
- off-topic
comments: false
---

Skyrim is an old game, but I hadn't really played it. I had it for the Xbox 360, but all I had done was run around collecting cabbages. I never made it passed Whiterun. It wasn't that I was bored, particularly, but other games held my attention a bit better.

I had heard, however, that the stuff that was available through mods was pretty awesome. When I finally took the time to build my own PC this year, gaming in general, and Skyrim modding in particular, were on my mind when I was choosing parts.

During the great Steam sale a few months ago, the Special Edition of Skyrim went on sale and I finally took the plunge. I was excited and decided to just start the vanilla game, making sure my specs matched my expectations and that the game ran properly before I started glomming stuff onto it.

## Oh no 🤢

I was disappointed ten minutes into my playthrough when I had to turn the game off because I was utterly sick to my stomach. The first-person camera gave me the most severe motion sickness I've ever had from a game.

I do occasionally get a bit ill from a game. Witcher 3 does this to me a little bit, Dragon Age II did it a little as well. With both of those games, an hour or so into the playthrough, my brain figured it out and I've been fine ever since. But those were third-person and I was never quite as sick.

## The solution

So began my hunt for a workaround. I wasn't alone apparently, because I was able to find plenty of references to the issue, especially in VR and various consoles, where it's much more difficult to create a solution. It took a bit of searching, but when I finally started up my fully modded game, I was able to play for a couple hours and walk away without even a tinge of nausea. Success!

Here's how I resolved my motion sickness problems in Skyrim SE. The only caveat is that if you've never installed mods before, take your time and read all the docs thoroughly. They can be tricky, but not impossible with a little patience. The mods and changes should be the same with the legacy version as well (these mods are ports of the older ones). The key to the whole thing for me was playing in third-person. I'll still go into first once in a while for archery aiming or for picking stuff up off a shelf, but with the mods added it's not bad for short periods.

## The mods

I started with [Mod Organizer 2](https://www.nexusmods.com/skyrimspecialedition/mods/6194) on a recommendation from a friend. This made it easy to handle the mods (it doesn't muck up your vanilla install!) and also lets you edit the `.ini` files directly from the interface. Nice. Note that if you're on Windows 10 you need to "run as administrator" or the mods won't be recognized.

Next came [Motion Sickness BE GONE](https://www.nexusmods.com/skyrimspecialedition/mods/2742) which removes all that camera bob stuff when you're moving around.

Because I needed to play in 3rd person, I grabbed the [3rd Person Camera Overhaul](https://www.nexusmods.com/skyrimspecialedition/mods/18515), which helps make this feel like Witcher, Dragon Age, etc. It's also configurable to let you auto switch to first-person if you're shooting arrows or something.

Okay, that's all you should need for the mods! Time to open up the preference files and make a few changes.

## The settings

In the Skyrimprefs.ini file, add/change `Bmouseacceleration=0` under `[controls]`. If it exists and it's `1` change it to `0` otherwise just add the whole line, minus the quote marks.

Then in the Skyrim.ini file, under `[Display]` , add `fDefaultWorldFOV=x.x` where `x.x` is the number of degrees of field of view you want. The default is `65`. I put it on `110` as suggested in various places.

## TADA!

You can now play third-person, motion-sickness-free Skyrim! You can use the scrollwheel to adjust the depth of field and also use that or hit `F` if you want to go first-person for something or are in a small space.

YMMV of course, but I went from literally seasick after 10 minutes of play to playing for about 4 hours with no issues.
