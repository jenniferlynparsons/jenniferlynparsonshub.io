---
layout: post
title:  "Talk Notes: Immutable Data Structures for Functional JS"
date:   2018-02-24
excerpt: "In this talk we’ll take a look at how these data structures work, why they’re fantastic for functional programming, and how we can easily use them in our JS code thanks to libraries like Mori and Immutable.js."
tag:
- talk notes
comments: false
---

## Talk
[**Immutable Data Structures for Functional JS**](https://www.youtube.com/watch?v=Wo0qiGPSV-s): Anjana Vakil, JSConf.EU `26:32`

## Summary

Functional programming has been gaining a lot of popularity in the JS community, and with good reason: rejecting side-effects and mutability - in-place changes to data - helps avoid a lot of headaches. But when you refuse to mutate objects, you have to create a whole new object each time something changes, which can slow things down and eat up memory, making functional programming seem inefficient. That’s where immutable data structures come in - to save the day, and time and space! Also called “persistent data structures”, they help you efficiently make new “modified” versions of immutable objects, by reusing parts of the old object that you don’t need to change. In this talk we’ll take a look at how these data structures work, why they’re fantastic for functional programming, and how we can easily use them in our JS code thanks to libraries like Mori and Immutable.js.

## My Notes

I loved this talk. I've watched one or two other of Anjana's talks and she's great, very passionate about her topics. I took away a slightly better understanding of how immutable data structures work, particularly in terms of functional JS. I'll be coming back to this talk again as I move deeper into a functional style of programming.