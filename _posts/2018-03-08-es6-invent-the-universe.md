---
layout: post
title:  "Talk Notes: If you wish to learn ES6/2015 from scratch, you must first invent the universe"
date:   2018-03-08
excerpt: "As we enter the era of language-level abstractions in ES6/2015, we are charged with the task of rethinking how we teach JavaScript."
tag:
- talk notes
comments: false
---

## Talk
[**If you wish to learn ES6/2015 from scratch, you must first invent the universe**](https://www.youtube.com/watch?v=DN4yLZB1vUQ): Ashley Williams, JSConf 2015 `25:48`

## Resources 
[Slides](http://ashleygwilliams.github.io/jsconf-2015-deck)

## Summary
Javascript has always been a language with very little syntactic sugar—for better or worse. With ES6/2015, and future iterations,though, Javascript is gaining a more and more abstract and expressive syntax. To some it might appear that our language—which already seems accessible and approachable for beginners— is becoming even more accessible and approachable. However, both the humanities and CS education research have proven that abstraction, while a powerful tool for knowledgeable practitioners, can be an equally powerful foil for beginners. As we enter the era of language-level abstractions in ES6/2015, we are charged with the task of rethinking how we teach JavaScript. Through an interdisciplinary montage Ashley will identify the problem of teaching abstraction as a ubiquitous demand across nearly every domain, and align the issues of creativity and critical thinking in the humanities with issues in computer science. The talk concludes with a discussion of how the discipline of computer science and that of the humanities can inform each other to produce more effective and creative solutions to both developing and teaching abstractions.

## My Notes
Holy wow. This talk is instantly in my top 3 favorites. Funny, intelligent, thought provoking, and it provided me with some solid vocabulary for a lot of the things that have bothered me about various language features but I was never really sure why I felt that way. Things that I was intuiting now how solidity to them. There's also a great bunch of references to various books I now have to go hunt down. 